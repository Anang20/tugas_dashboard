<?php

$api = curl_init();
curl_setopt($api, CURLOPT_URL, 'https://api.covid19api.com/summary');
curl_setopt($api, CURLOPT_RETURNTRANSFER, true);
$result = curl_exec($api);
curl_close($api);
$result = json_decode($result, true);
$country = $result['Countries'];
$nomor = 1;
?>

<!DOCTYPE html>
<html>
<head>
	<title>Data Terbaru</title>
	<!--Import icon fontawesome-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">
    <!--Import dari materialize.css-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <!-- Import faficon -->
    <link rel="shortcut icon" href="image/coronavirus.png" type="image/x-icon" class="rounded-circle">
    <script src="https://code.jquery.com/jquery-3.1.0.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

</head>
<style type="text/css">
body {
    background-image: linear-gradient(to top left,#8a4592,#35b0a9);
}
.bg{
	background: linear-gradient(to top left,#8a4592,#35b0a9);
}
nav{
	background-image: linear-gradient(to top right,#35b0a9,#8a4592); 
	padding-left: 300px;
}
.content{
	padding-left: 300px;
	height: 800px;
}
.card-bg{
	background: rgba(0,0,0,0);
}
@media only screen and (max-width: 992px){
	.content,nav{
		padding-left: 0;
	}
}
</style>

    <!-- Slide out -->
<script type="text/javascript">
	$(document).ready(function(){
		$('.sidenav').sidenav();
	});
</script>

<body>

    <!-- Navbar -->

	<div class="navbar-fixed">
        <nav>
            <div class="nav-wrapper">
                <a href="#" class="brand-logo center">Latest Data</a>
                <a href="#" data-target="slide-out" class="sidenav-trigger"><i class="fas fa-bars fa-2x"></i></a>		
            </div>
        </nav>
    </div>
    
    <!-- Sidenav -->

	<ul class="sidenav sidenav-fixed bg" id="slide-out">
		<li>
			<div class="user-view">
				<div class="background">
					<img src="image/bg.jpg" width="100%">
				</div>
				<a href="https://github.com/Anang20"><img src="image/pp.png" class="circle"></a>
				<a href="https://github.com/Anang20" class="white-text name">Anang Syah Amirul Haqim</a>
				<a href="mailto:anangsyah766@gmail.com" class="white-text email">anangsyah766@gmail.com</a>
			</div>	
		</li>
		<li><a href="index.php" class="white-text"><i class="fas fa-home fa-2x"></i>Dashboard</a></li>
        <li><a href="data_terbaru.php" class="white-text"><i class="far fa-calendar-check fa-2x"></i>Latest Data</a></li>
		<li><a href="data.php" class="white-text"><i class="fas fa-globe-americas fa-2x"></i>Data Per Country</a></li>
	</ul>
    
    <!-- Table -->

<div class="content bg white-text">
    <div class="row">
        <div class="col s12">
            <div class="card card-bg">
                    <table>
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Country</th>
                            <th>New Confirmed</th>
                            <th>New Deaths</th>
                            <th>New Recovered</th>
                        </tr>
                        </thead>
                        <?php foreach ($country as $key): ?>
                        <tbody>
                        <tr>
                            <td><?= $nomor++."." ?></td>
                            <td><?= $key['Country'] ?></td>
                            <td><?= number_format( $key['NewConfirmed']) ?></td>
                            <td><?= number_format($key['NewDeaths']) ?></td>
                            <td><?= number_format($key['NewRecovered']) ?></td>
                        </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
            </div>
        </div>
    </div>
</div>

</body>
</html>