<?php

$api = curl_init();
curl_setopt($api, CURLOPT_URL, 'https://api.covid19api.com/summary');
curl_setopt($api, CURLOPT_RETURNTRANSFER, true);
$result = curl_exec($api);
curl_close($api);
$result = json_decode($result, true);
$countries = $result['Countries'];
$total_countries = count($countries);
$global = $result['Global'];
$all = $global;
$persentase1 = $all["NewConfirmed"]/$all["TotalConfirmed"]*100;
$persentase2 = $all["NewDeaths"]/$all["TotalConfirmed"]*100;
$persentase3 = $all["TotalDeaths"]/$all["TotalConfirmed"]*100;
$persentase4 = $all["NewRecovered"]/$all["TotalConfirmed"]*100;
$persentase5 = $all["TotalRecovered"]/$all["TotalConfirmed"]*100;
?>

<!DOCTYPE html>
<html>
<head>
	<title>Dashboard</title>
	<!--Import icon fontawesome-->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">
    <!--Import dari materialize.css-->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
	<!-- Import faficon -->
	<link rel="shortcut icon" href="image/coronavirus.png" type="image/x-icon" class="rounded-circle">
    <script src="https://code.jquery.com/jquery-3.1.0.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
</head>
<style type="text/css">
body {
	background-image: linear-gradient(to top left,#8a4592,#35b0a9);
}

.bg{
	background-image: linear-gradient(to top left,#8a4592,#35b0a9);
}

nav{
	background-image: linear-gradient(to top right,#35b0a9,#8a4592); 
	padding-left: 300px;
}

.content{
	padding-left: 300px;
	height:100%;
}

.card-bg{
	background: rgba(0,0,0,0);
}

@media only screen and (max-width: 1000px){
	.content,nav{
		padding-left: 0;
	}
}

</style>

<!-- slide out -->

<script type="text/javascript">
	$(document).ready(function(){
		$('.sidenav').sidenav();
	});
</script>

<body>

	<!-- Navbar -->

	<div class="navbar-fixed">
		<nav>
			<div class="nav-wrapper">
				<a href="#" class="brand-logo center">DashBoard</a>
				<a href="#" data-target="slide-out" class="sidenav-trigger"><i class="fas fa-bars fa-2x"></i></a>
			</div>
		</nav>
	</div>

	<!-- Sidenav -->

	<ul class="sidenav sidenav-fixed bg" id="slide-out">
		<li>
			<div class="user-view">
				<div class="background">
					<img src="image/bg.jpg" width="100%">
				</div>
				<a href="https://github.com/Anang20"><img src="image/pp.png" class="circle"></a>
				<a href="https://github.com/Anang20" class="white-text name">Anang Syah Amirul Haqim</a>
				<a href="mailto:anangsyah766@gmail.com" class="white-text email">anangsyah766@gmail.com</a>
			</div>	
		</li>
		<li><a href="index.php" class="white-text"><i class="fas fa-home fa-2x"></i>Dashboard</a></li>
		<li><a href="data_terbaru.php" class="white-text"><i class="far fa-calendar-check fa-2x"></i>Latest Data</a></li>
		<li><a href="data.php" class="white-text"><i class="fas fa-globe-americas fa-2x"></i>Data Per Country</a></li>
	</ul>

	<!-- Cards -->

	<div class="content bg">
		<div class="container">
			<div class="row">
				<div class="col l12 m6 s12">
					<div class="card card-bg">
						<div class="card-content">
							<b class="white-text"><?php echo "terakhir di update pada tanggal : ".$countries[0]["Date"]; ?></b>		
						</div>
					</div>
				</div>
				<div class="col s12 m6 l3">
					<div class="card card-bg white-text">
						<div class="card-content center">
							<p>Virus Case</p>
							<h5>Corona</h5>
							<b class="red-text">Virus</b>
						</div>
					</div>
				</div>
				<div class="col s12 m6 l3">
					<div class="card card-bg white-text">
						<div class="card-content center">
							<p>Total Countries</p>
							<h5><?= $total_countries ?></h5>
							<b class="yellow-text">Country</b>
						</div>
					</div>
				</div>
				<div class="col s12 m6 l3">
					<div class="card card-bg white-text">
						<div class="card-content center">
							<p>New Confirmed</p>
							<h5><?= number_format($global["NewConfirmed"]) ?></h5>
							<b class="orange-text"><?= substr($persentase1, 0, 4)."%" ?></b>
						</div>
					</div>
				</div>
				<div class="col s12 m6 l3">
					<div class="card card-bg white-text">
						<div class="card-content center">
							<p>Total Confirmed</p>
							<h5><?= number_format($global["TotalConfirmed"]) ?></h5>
							<b class="green-text">100%</b>
						</div>
					</div>
				</div>
				<div class="col s12 l3 m6">
					<div class="card card-bg white-text">
						<div class="card-content center">
							<p>New Deaths</p>
							<h5><?= number_format($global["NewDeaths"]) ?></h5>
							<b class="red-text"><?= substr($persentase2, 0, 4)."%" ?></b>
						</div>
					</div>
				</div>
				<div class="col s12 l3 m6">
					<div class="card card-bg white-text">
						<div class="card-content center">
							<p>Total Deaths</p>
							<h5><?= number_format($global["TotalDeaths"]) ?></h5>
							<b class="orange-text"><?= substr($persentase3, 0, 4)."%" ?></b>
						</div>
					</div>
				</div>
				<div class="col s12 l3 m6">
					<div class="card card-bg white-text">
						<div class="card-content center">
							<p>New Recovered</p>
							<h5><?= number_format($global["NewRecovered"]) ?></h5>
							<b class="red-text"><?= substr($persentase4, 0, 4)."%" ?></b>
						</div>
					</div>
				</div>
				<div class="col s12 l3 m6">
					<div class="card card-bg white-text">
						<div class="card-content center">
							<p>Total Recovered</p>
							<h5><?= number_format($global["TotalRecovered"]) ?></h5>
							<b class="green-text"><?= substr($persentase1, 0, 4)."%" ?></b>
						</div>
					</div>
				</div>
				<div class="col l12 m6 s12">
					<div class="card card-bg">
						<div class="card-content">
							<h4 class="white-text" style="text-align: center;">Total Confirmed</h4>
							<canvas id="myChart" style="height: 600px; width: 900px"></canvas>
						</div>
					</div>
				</div>
				<div class="col l12 m6 s12">
					<div class="card card-bg">
						<div class="card-content">
							<h4 class="white-text" style="text-align: center;">Total Deaths</h4>
							<canvas id="myChart2" style="height: 600px; width: 900px"></canvas>
						</div>
					</div>
				</div>
				<div class="col l12 m6 s12">
					<div class="card card-bg">
						<div class="card-content">
							<h4 class="white-text" style="text-align: center;">Total Recovered</h4>
							<canvas id="myChart3" style="height: 600px; width: 900px"></canvas>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Chart -->
	
	<script type="text/javascript">
		var chr=document.getElementById("myChart").getContext("2d");
		var chr2=document.getElementById("myChart2").getContext("2d");
		var chr3=document.getElementById("myChart3").getContext("2d");

		var covid = $.ajax({
            url: "https://api.covid19api.com/summary",
            cache : false
        })
		
		.done(function (canvas) {
            
                
            function getContries(canvas) {
                var show_country=[];

                canvas.Countries.forEach(function(el) {
                    show_country.push(el.Country);
                })
                return show_country;
            }


            function getConfirmed(canvas) {
                var confirmed=[];

                canvas.Countries.forEach(function(el) {
                    confirmed.push(el.TotalConfirmed)
                })
                return confirmed;
            } 

			function getDeaths(canvas) {
                var deaths=[];

                canvas.Countries.forEach(function(el) {
                    deaths.push(el.TotalDeaths)
                })
                return deaths;
            } 

			function getRecovered(canvas) {
                var recovered=[];

                canvas.Countries.forEach(function(el) {
                    recovered.push(el.TotalRecovered)
                })
                return recovered;
            } 
            
            var color=[];
            function color_random(){
                var r = Math.floor(Math.random() * 255);
                var g = Math.floor(Math.random() * 255);
                var b = Math.floor(Math.random() * 255);
                return "rgb(" + r + "," + g + "," + b +")";
            }

            for ( var i in canvas.Countries) {
                color.push(color_random());
            }

            var myChart = new Chart(chr,{
                type: 'pie',
                data: {
                    labels: getContries(canvas),
                    datasets : [{
                        label: 'terkonfirmasi',
                        data: getConfirmed(canvas),
                        backgroundColor: color,
                        borderColor:color,
                        borderWidth: 1,
                    }]
                },
				options:{
				legend:{
					display: false
				}
			}
            })

			var myChart = new Chart(chr2,{
                type: 'pie',
                data: {
                    labels: getContries(canvas),
                    datasets : [{
                        label: 'Meninggal',
                        data: getDeaths(canvas),
                        backgroundColor: color,
                        borderColor:color,
                        borderWidth: 1,
                    }]
                },
				options:{
				legend:{
					display: false
				}
			}
            })
			var myChart = new Chart(chr3,{
                type: 'pie',
                data: {
                    labels: getContries(canvas),
                    datasets : [{
                        label: 'Sembuh',
                        data: getRecovered(canvas),
                        backgroundColor: color,
                        borderColor:color,
                        borderWidth: 1,
                    }]
                },
				options:{
				legend:{
					display: false
				}
			}
            })
        });
	</script>
</body>
</html>